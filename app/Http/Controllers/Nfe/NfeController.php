<?php


namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Services\NfeGetCaptchaService;
use App\Services\NfeResolveCaptchaService;

class NfeController extends BaseController
{
    /**
     * NfeController
     */
    
    var $getCaptchaService;
    var $resolveCaptchaService;

    function __construct()
    {
        $this->getCaptchaService = new NfeGetCaptchaService();
        $this->resolveCaptchaService = new NfeResolveCaptchaService();
    }

    public function index()
    {
        return '<img src="'. $this->getCaptchaService->getCaptcha().'"/>';
        // var_dump($this->getCaptchaService->getCaptcha());
    }

    public function getCaptcha()
    {
        return var_dump($this->getCaptchaService->getCaptcha());
    }

    
    public function resolveCaptcha()
    {
         return ($this->getCaptchaService->getCaptcha());
    }
}
