<?php


namespace App\Services;

use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

class NfeGetCaptchaService
{
    var $crawller;
    function __construct()
    {
        $this->crawller =  $_SESSION['web.crawller'];
        // Create Spider
        if (isset($this->crawller->pageInstance)) {
            $client = new Client();
            $c = $client->request('GET', $this->crawller->uri);
            $this->crawller->pageInstance = $c;
            $_SESSION['web.crawller'] = $this->crawller;
        }
    }


    function getCaptcha()
    {
        return $_SESSION['web.crawller']->pageInstance;
    }
}
